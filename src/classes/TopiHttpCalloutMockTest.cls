/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class TopiHttpCalloutMockTest {

    @isTest
    public static void givenMockWhenInitiate() {
        String payload = '{ "key" : "value" }';
        HttpResponse response = new TopiHttpCalloutMock( payload, 200).respond( new HttpRequest() );

        System.assertEquals( '{ "key" : "value" }', response.getBody(), 'Escpected payload');
        System.assertEquals( 200, response.getStatusCode(), 'Escpected 200');
    }
}