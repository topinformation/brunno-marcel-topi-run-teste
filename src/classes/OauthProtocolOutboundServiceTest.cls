/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class OauthProtocolOutboundServiceTest {

    @isTest
    static void givenOauthWhenSuccessCallServiceThenReturnToken() {
        String payload = '{ "access_token": "00D4W0000048AiQ!AQoAQKgVN6E4i3uIec7RcHjSHfFcknDSEU6D2QKFybFmMrW566TwI1YxcA0aEEeVVsvfAUaUHv5r1PAZzPaFfPQ.XuImOXGt", "instance_url": "https://bisso-topirun-20-dev-ed.my.salesforce.com", "id": "https://login.salesforce.com/id/00D4W0000048AiQUAU/0054W00000B99edQAB", "token_type": "Bearer", "issued_at": "1601639269587", "signature": "Y4PFBhk+5IhmznVJobqlK6rK8ggSOD41seY4auvYJzA=" }';
        Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 200 ) );

        Test.startTest();
        String token = new OauthProtocolOutboundService().requestToken();
        Test.stopTest();

        System.assertEquals( '00D4W0000048AiQ!AQoAQKgVN6E4i3uIec7RcHjSHfFcknDSEU6D2QKFybFmMrW566TwI1YxcA0aEEeVVsvfAUaUHv5r1PAZzPaFfPQ.XuImOXGt', token, 'Correct token');
    }

    @isTest
    static void givenOauthWhenFailCallServiceThenReturnCalloutException() {
        String payload = '{ "erro": "erro" }';
        Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 400 ) );

        Test.startTest();
        try {
            String token = new OauthProtocolOutboundService().requestToken();
            System.assert( false , 'Expected CalloutException');
        } catch (CalloutException ex) {
            System.assert( true , 'Expected CalloutException');
        }
        Test.stopTest();
    }
}