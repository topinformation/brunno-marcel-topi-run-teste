/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GeolocationAddressSuccessResponseTest {

    @isTest
    static void givenGeolocationAddressMappingSuccessResponseWhenInsertNewZipcodeThenCheckInstanceZipCodeValue() {
        GeolocationAddressMappingSuccessResponse geolocationSucessResponse = new GeolocationAddressMappingSuccessResponse();

        geolocationSucessResponse.billingAddress.latitude = '-49.2733';

        System.assertEquals( '-49.2733', geolocationSucessResponse.billingAddress.latitude, 'Expeceted equals values' );
    }
}