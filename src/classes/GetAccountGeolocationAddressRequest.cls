/**
 * @author Brunno Marcel
 */
public class GetAccountGeolocationAddressRequest {
    public List<String> externalIds;

    public GetAccountGeolocationAddressRequest() {
        externalIds = new List<String> ();
    }
}