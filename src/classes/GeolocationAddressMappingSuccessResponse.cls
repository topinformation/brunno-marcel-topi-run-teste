/**
 * @author Brunno Marcel -TOPi
 */
public class GeolocationAddressMappingSuccessResponse {

    public String name;
    public String externalId;
    public Geolocation billingAddress;
    public Geolocation shippingAddress;

    public GeolocationAddressMappingSuccessResponse() {
        this.billingAddress = new GeolocationAddressMappingSuccessResponse.Geolocation();
        this.shippingAddress = new GeolocationAddressMappingSuccessResponse.Geolocation();
    }

    public class Geolocation {
        public String latitude;
        public String longitude;
    }
}