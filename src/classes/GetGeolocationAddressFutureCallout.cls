/** 
 * @author Brunno Marcel - TOPi
 */
public class GetGeolocationAddressFutureCallout {

    @future( callout = true )
    public static void getAccountGeolocationAddress( List<String> accountIds ) {

        AccountRepository accountRepository = new AccountRepository();
        List<Account> accounts = accountRepository.findByIds( accountIds );

        accounts = new GetGeolocationAddressOutboundService().getGeolocationAddress( accounts );

        accountRepository.store( accounts );
    }
}