/**
 * @author Brunno Marcel - TOPi
 */
public class CpfFormatter {
    public class InvalidInputException extends Exception {}

    public static String withMask( String cpf ) {
        cpf = withoutMask( cpf );
        if ( cpf.length() != 11 ) {
            throw new InvalidInputException( 'CPF should have length 11 numbers' );
        }
        return cpf.replaceAll( '(\\d{3})(\\d{3})(\\d{3})(\\d{2})', '$1.$2.$3-$4' );
    }

    public static String withoutMask( String cpf ){
        if ( String.isEmpty( cpf )) {
            return '';
        }
        return cpf.replaceAll( '\\.', '' ).replaceAll( '-', '' );
    }
}