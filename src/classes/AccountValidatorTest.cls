/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountValidatorTest {

    @isTest
    public static void givenAccountsWhenValidateThenReturnAccountsWithValidCpfAndCnpj() {
        List<Account> accounts = new List<Account>();
        accounts.add( AccountFixtureFactory.newDefaultAccount() );

        AccountValidator validator = new AccountValidator();
        List<Account> validetedAccounts = validator.validateCpfCnpj( accounts );

        System.assertEquals( validetedAccounts, accounts, 'Expected equals values' );
        System.assert( validetedAccounts.size() == 1, 'Expected one row' );
    }

    @isTest
    public static void givenAccountsWithInvalidCpfWhenValidateAndTrySaveThenTrowDmlException() {
        Account account = AccountFixtureFactory.newDefaultAccount();
        account.Cpf__c = '00000000000';
        
        List<Account> accounts = new List<Account>();
        accounts.add( account );
        
        AccountValidator validator = new AccountValidator();
        
        try {
            List<Account> validetedAccounts = validator.validateCpfCnpj( accounts );
            AccountFixtureFactory.create( validetedAccounts );
            System.assert( false, 'Expected exception' );
        }
        catch(DmlException e) {
            System.Assert( true, 'Expected exception' );
        }
    }
    
    @isTest
    public static void givenAccountsWithInvalidCnpjWhenValidateAndTrySaveThenTrowDmlException() {
        Account account = AccountFixtureFactory.newDefaultAccount();
        account.Cnpj__c = '00000000000000';

        List<Account> accounts = new List<Account>();
        accounts.add( account );

        AccountValidator validator = new AccountValidator();
        try {
            List<Account> validetedAccounts = validator.validateCpfCnpj( accounts );
            AccountFixtureFactory.create( validetedAccounts );
            System.assert( false, 'Expected exception' );
        } catch(DmlException e) {
            System.Assert( true, 'Expected exception' );
        }
    }
}