/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountFilterTest {

    @isTest
    public static void givenAccountsWhenFilterNotEmptyCpfOrCnpjThenReturnAccountsWithNotEmptyCpfOrCnpj() {
        List<Account> accounts = new List<Account>();
        accounts.add( AccountFixtureFactory.newDefaultAccount() );
        accounts.add( AccountFixtureFactory.newAccount( 'TEST 01', '') );

        List<Account> filteredAccounts = new AccountFilter().filterByNotEmptyCpfOrCnpj( accounts );

        System.assertNotEquals( filteredAccounts, accounts, 'Expected different accounts' );
        System.assert( filteredAccounts.size() == 1, 'Expected one filtered row' );
    }

    @isTest
    public static void givenOldAccountsWhenFilterNewBillingAddressThenReturnAccountsWithNewBillingAddress() {
        Account account = AccountFixtureFactory.newDefaultAccount();

        Map<Id, Account> oldAccounts = new Map<Id, Account>();
        oldAccounts.put( account.Id, account.clone( true, true, true, true) );
        oldAccounts.put( AccountFixtureFactory.newDefaultAccount().Id, AccountFixtureFactory.newDefaultAccount() );
        
        account.BillingStreet = 'Boulevard';
        account.BillingPostalCode = '84020-030';
        
        List<Account> newAccounts = new List<Account>();
        newAccounts.add( account );
        newAccounts.add( AccountFixtureFactory.create( 'TEST 01', '' ) );
        newAccounts.add( AccountFixtureFactory.newDefaultAccount() );

        List<Account> filteredAccounts = new AccountFilter().filterByNewBillingAddress( newAccounts, oldAccounts );

        System.assert( filteredAccounts.size() == 1, 'Expected one filtered row' );
    }

    @isTest
    public static void givenOldAccountsWhenFilterNewShippingAddressThenReturnAccountsWithNewShippingAddress() {
        Account account = AccountFixtureFactory.newDefaultAccount();
        
        Map<Id, Account> oldAccounts = new Map<Id, Account>();
        oldAccounts.put( account.Id, account.clone( true, true, true, true) );
        oldAccounts.put( AccountFixtureFactory.newDefaultAccount().Id, AccountFixtureFactory.newDefaultAccount() );

        account.ShippingStreet = 'Boulevard';
        account.ShippingPostalCode = '84020-030';

        List<Account> newAccounts = new List<Account>();
        newAccounts.add( account );
        newAccounts.add( AccountFixtureFactory.create( 'TEST 01', '' ) );
        newAccounts.add( AccountFixtureFactory.newDefaultAccount() );

        List<Account> filteredAccounts = new AccountFilter().filterByNewShippingAddress( newAccounts, oldAccounts );

        System.assert( filteredAccounts.size() == 1, 'Expected one filtered row' );
    }

    @isTest
    public static void givenAccountsWhenParseAccountsToIdsThenReturnListOfIds() {
        Account account = AccountFixtureFactory.newDefaultAccount();
        
        List<Account> accounts = new List<Account>(); 
        accounts.add( account );

        Set<Account> uniqueAccounts = new Set<Account>();
        uniqueAccounts.addAll( accounts );

        List<String> filteredAccounts = new AccountFilter().parseToIds( uniqueAccounts );

        System.assertEquals( filteredAccounts[0], accounts[0].Id, 'Expected equals ids' );
        System.assert( filteredAccounts.size() == 1, 'Expected one filtered row' );
    }
}