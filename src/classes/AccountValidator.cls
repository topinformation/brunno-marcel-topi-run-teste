/**
 * @author Brunno Marcel - TOPi
 */
public class AccountValidator {

    public List<Account> validateCpfCnpj( List<Account> accounts ) {
        BrazilianDocumentValidator validator = new BrazilianDocumentValidator();

        for ( Account account : accounts ) {
            if ( String.isNotEmpty( account.Cpf__c ) && !validator.isCPF( CpfFormatter.withoutMask( account.Cpf__c ) ) ){
                account.Cpf__c.addError( 'Inválido' );
            }
            if ( String.isNotEmpty( account.Cnpj__c ) && !validator.isCNPJ( CnpjFormatter.withoutMask( account.Cnpj__c ) ) ) {
                account.Cnpj__c.addError( 'Inválido' );
            }
        }

        return accounts;
    }

}