/**
 * @author Brunno Marcel - Topi
 */
@isTest
public class GeolocationAddressMappingTransformerTest {

    @isTest
    public static void givenAccountWhenTransformToRequestThenCheckValues() {
        Account account = AccountFixtureFactory.newDefaultAccount();

        GeolocationAddressMappingRequest request = new GeolocationAddressMappingTransformer().accountToGeolocationRequest( account );

        System.assertEquals( 'Conta Cpf Cnpj', request.name, 'Expected equals' );
        System.assertEquals( 'Main Street', request.billingAddress.street, 'Expected equals' );
        System.assertEquals( '84030-030', request.billingAddress.zipCode, 'Expected equals' );
        System.assertEquals( 'Main Street', request.shippingAddress.street, 'Expected equals' );
        System.assertEquals( '84030-030', request.shippingAddress.zipCode, 'Expected equals' );
    }

    @isTest
    public static void givenAccountAndErrorResponseWhenTransformReponseToAccountThenCheckValues() {
        Account account = AccountFixtureFactory.newDefaultAccount();

        GeolocationAddressMappingErrorResponse errorResponse = new GeolocationAddressMappingErrorResponse();
        errorResponse.message = 'error 123';
        errorResponse.errorCode = '123';

        List<GeolocationAddressMappingErrorResponse> errorResponses = new List<GeolocationAddressMappingErrorResponse>();
        errorResponses.add( errorResponse );

        account = new GeolocationAddressMappingTransformer().responseToAccount( account, errorResponses );

        System.assert( account.GeolocationRequested__c, 'Expected true' );
        System.assertEquals( 'error 123', account.GeolocationErrorMessage__c, 'Expected equals' );
    }
}