/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GeolocationSuccessResponseFixtureFactory {

    public static GeolocationAddressMappingSuccessResponse newDefaultGeolocationAddressMappingSuccessResponse() {
        String payload = '{"shippingAddress": {"zipCode": "14350-970","street": "Rua Coronel Honório Palma 135","state": "SP","longitude": "-47.37663","latitude": "-21.02352","country": "Brasil","city": "Altinópolis"},"name": "Teste do Bisso","externalId": "0015Y00002bBoIvQAK","billingAddress": {"zipCode": "18770-970","street": "Rua Pedro Dias Batista 70","state": "SP","longitude": "-49.24069129319908","latitude": "-22.880184142428277","country": "Brasil","city": "Águas de Santa Bárbara"}}';
        return fromJson( payload );
    }

    public static GeolocationAddressMappingSuccessResponse fromJson( String payload ) {
        return ( GeolocationAddressMappingSuccessResponse ) JSON.deserialize( payload, GeolocationAddressMappingSuccessResponse.class );
    }
}