/**
 * @author Brunno Marcel - TOPi
 */
public class AccountFormatter {

    public List<Account> formatData( List<Account> accounts ) {

        for(Account account: accounts){
            if( String.isNotEmpty( account.Cpf__c ) ) {
                account.CPF__c = CpfFormatter.withMask(account.CPF__c);
            }
            if( String.isNotEmpty( account.Cnpj__c ) ) {
                account.CNPJ__c = CnpjFormatter.withMask(account.CNPJ__c);
            }
            if( String.isNotEmpty(account.BillingPostalCode) ) {
                account.BillingPostalCode = CepFormatter.withMask(account.BillingPostalCode);
            }
            if( String.isNotEmpty(account.ShippingPostalCode) ) {
                account.ShippingPostalCode = CepFormatter.withMask(account.ShippingPostalCode);
            }
        }

        return accounts;
    }
}