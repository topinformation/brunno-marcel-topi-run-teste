/**
 * @author Brunno Marcel - TOPi
 */
public class CnpjFormatter {
    public class InvalidInputException extends Exception {}

    public static String withMask( String cnpj ) {
        cnpj = withoutMask( cnpj );
        if ( cnpj.length() != 14 ) {
            throw new InvalidInputException( 'CNPJ should have length 14 numbers' );
        }
        return cnpj.replaceAll( '(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})', '$1.$2.$3/$4-$5' );
    }

    public static String withoutMask( String cnpj ){
        if ( String.isEmpty( cnpj )) {
            return '';
        }
        return cnpj.replaceAll( '\\.', '' ).replaceAll( '/', '' ).replaceAll( '-', '' );
    }
}