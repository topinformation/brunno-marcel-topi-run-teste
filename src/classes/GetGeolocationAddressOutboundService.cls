/** 
 * @author Brunno Marcel - TOPi
 */
public class GetGeolocationAddressOutboundService {

    public static final String END_POINT = 'https://bisso-topirun-20-dev-ed.my.salesforce.com/services/apexrest/api/account/geolocation';
    public GetAccountGeolocationAddressTransformer transformer;

    public GetGeolocationAddressOutboundService() {
        this.transformer = new GetAccountGeolocationAddressTransformer();
    }

    public List<Account> getGeolocationAddress( List<Account> accounts ) {
        Http http = new Http();
        HttpResponse response = http.send( generateRequest( accounts ) );

        if ( response.getStatusCode() != 200 ) {
            List<GeolocationAddressMappingErrorResponse> errorResponses = ( List<GeolocationAddressMappingErrorResponse> ) JSON.deserialize( response.getBody(), List<GeolocationAddressMappingErrorResponse>.class );
            return transformer.responseToAccount( new Map<Id, Account>( accounts ) , errorResponses );
        }

        List<GeolocationAddressMappingSuccessResponse> sucessResponses = ( List<GeolocationAddressMappingSuccessResponse> ) JSON.deserialize( response.getBody(), List<GeolocationAddressMappingSuccessResponse>.class );
        return transformer.responseToAccount( new Map<Id, Account>( accounts ) , sucessResponses );
    }

    public HttpRequest generateRequest( List<Account> accounts ) {
        HttpRequest request = new HttpRequest();
        String accessToken  = new OauthProtocolOutboundService().requestToken();

        request.setEndpoint( END_POINT );
        request.setMethod( 'PATCH' );
        request.setHeader( 'Authorization', 'Bearer ' + accessToken  );
        request.setHeader( 'Content-Type', 'application/json' );
        request.setBody( JSON.serialize( transformer.accountToGeolocationRequest( accounts ) ) );

        return request;
    }
}