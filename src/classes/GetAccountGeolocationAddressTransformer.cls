/**
 * @author Brunno Marcel -TOPi
 */
public class GetAccountGeolocationAddressTransformer {

    public GetAccountGeolocationAddressRequest accountToGeolocationRequest( List<Account> accounts ) {
        GetAccountGeolocationAddressRequest request = new GetAccountGeolocationAddressRequest();

        for (Account account : accounts) {
            request.externalIds.add( account.Id );
        }
        
        return request;
    }
    
    public List<Account> responseToAccount( Map<Id, Account> mapAccounts, List<GeolocationAddressMappingSuccessResponse> successResponses ) {
        List<Account> accounts = new List<Account>();

        for (GeolocationAddressMappingSuccessResponse successResponse : successResponses) {
            Account account = mapAccounts.get( successResponse.externalId );

            account.GeolocationRequested__c = false;
            if (String.isNotEmpty( successResponse.billingAddress.latitude )) {
                account.BillingLatitude = Decimal.valueOf( successResponse.billingAddress.latitude );
            }
            if (String.isNotEmpty( successResponse.billingAddress.longitude )) {
                account.BillingLongitude = Decimal.valueOf( successResponse.billingAddress.longitude );
            }
            if (String.isNotEmpty( successResponse.shippingAddress.latitude )) {
                account.ShippingLatitude = Decimal.valueOf( successResponse.shippingAddress.latitude );
            }
            if (String.isNotEmpty( successResponse.shippingAddress.longitude )) {
                account.ShippingLongitude = Decimal.valueOf( successResponse.shippingAddress.longitude );
            }
            account.GeolocationErrorMessage__c = '';

            accounts.add( account );
        }

        return accounts;
    }
    
    public List<Account> responseToAccount( Map<Id, Account> mapAccounts, List<GeolocationAddressMappingErrorResponse> errorResponses ) {
        List<Account> accounts = new List<Account>();

        for (Account account : mapAccounts.values()) {
            account.GeolocationRequested__c = true;
            account.GeolocationErrorMessage__c = errorResponses[0].message;
            
            accounts.add( account );
        }

        return accounts;
    }
}