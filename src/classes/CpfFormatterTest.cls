/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class CpfFormatterTest {

    @isTest
public static void givenCpfWhenPutMaskThenReturnCpfWithMask(){
        String formatedCpf = CpfFormatter.withMask( '00000000000' );
        System.assertEquals( '000.000.000-00', formatedCpf, 'Expeceted formatted values' );
    }

    @isTest
    public static void givenInvalidCpfWhenPutMaskThenTrowInvalidInputException(){
        try {
            CpfFormatter.withMask( '000000000' );
            System.assert( false, 'It should throw exception' );
        } catch (CpfFormatter.InvalidInputException ex) {
            System.assert( ex.getMessage() == 'CPF should have length 11 numbers', 'Expeceted equals values' );
        }
    }

    @isTest
    public static void givenCpfWhenRemoveMaskThenReturnCpfWithoutMask(){
        String formatedCpf = CpfFormatter.withoutMask( '000.000.000-00' );
        System.assertEquals( '00000000000', formatedCpf, 'Expeceted unformated value' );
    }

    @isTest
    public static void givenNullCpfWhenRemoveMaskThenReturnCpfWithoutMask(){
        String formatedCpf = CpfFormatter.withoutMask( null );
        System.assertEquals( '', formatedCpf, 'Expeceted blank value' );
    }
}