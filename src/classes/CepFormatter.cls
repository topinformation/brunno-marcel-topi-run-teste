/**
 * @author Brunno Marcel - TOPi
 */
public class CepFormatter {
    public class InvalidInputException extends Exception {}
    
    public static String withMask( String cep ) {
        cep = withoutMask( cep );
        if ( cep.length() != 8 ) {
            throw new InvalidInputException( 'CEP should have length 8 numbers' );
        }
        return cep.replaceAll( '(\\d{5})(\\d{3})', '$1-$2' );
    }

    public static String withoutMask( String cep ){
        if ( String.isEmpty( cep )) {
            return '';
        }
        return cep.replaceAll( '-', '' );
    }
}