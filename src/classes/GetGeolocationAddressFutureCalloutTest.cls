/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GetGeolocationAddressFutureCalloutTest {

    @isTest
    static void givenAccountsWithAddressWhenSuccessCallFutureCalloutThenUpdatedGeolocationRequestedToTrue() {
        String payload = '{[{"shippingAddress": {"zipCode": "14350-970","street": "Rua Coronel Honório Palma 135","state": "SP","longitude": "-47.37663","latitude": "-21.02352","country": "Brasil","city": "Altinópolis"},"name": "Teste do Bisso","externalId": "23423423432423","billingAddress": {"zipCode": "18770-970","street": "Rua Pedro Dias Batista 70","state": "SP","longitude": "-49.24069129319908","latitude": "-22.880184142428277","country": "Brasil","city": "Águas de Santa Bárbara"}}]}';
        Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 200 ) );

        Account account = AccountFixtureFactory.newDefaultAccount();
        account.Name = 'Teste GeolocationRequested__c false';
        account.Id = null;
        account.GeolocationRequested__c = false;
        AccountFixtureFactory.create(account);
        
        List<Account> accounts = new List<Account>(); 
        accounts.add( account );

        Test.startTest();
        GetGeolocationAddressFutureCallout.getAccountGeolocationAddress( new AccountFilter().parseToIds( accounts ) );
        Test.stopTest();

        accounts.clear();
        accounts = AccountFixtureFactory.findByName( 'Teste GeolocationRequested__c false' );

        System.assert( accounts[0].GeolocationRequested__c, 'Expected true' );
    }
}