/**
 * @author Brunno Marcel - TOPi
 */
public virtual class Filter {

    virtual
    public List<String> parseToIds( List<SObject> params ) {
        List<String> ids = new List<String>();

        for (SObject param : params) {
            ids.add( param.Id );
        }

        return ids;
    }
}