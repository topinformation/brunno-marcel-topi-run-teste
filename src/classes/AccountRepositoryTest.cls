/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountRepositoryTest {

    @isTest
    public static void givenAccountsWhenUpdatedThenSelectAccountsUpdatedByIds() {
        List<Account> accounts = new List<Account>();
        accounts.add( AccountFixtureFactory.create( 'TEST REPOSITORY', '' ) );

        AccountRepository repository = new AccountRepository();
        repository.store( accounts );

        List<Account> persistedAccounts = repository.findByIds( new AccountFilter().parseToIds( accounts ) );

        System.assertEquals( persistedAccounts[0].Id, accounts[0].Id, 'Expected equals ids' );
        System.assert( persistedAccounts.size() == 1, 'Expected one row' );
    }

    @isTest
    public static void givenAccountsWhenFindByPendingGeolocationRequestThenReturn() {
        Account account = AccountFixtureFactory.create( 'TEST REPOSITORY', '' );
        account.GeolocationRequested__c = true;
        
        List<Account> accounts = new List<Account>();
        accounts.add( account );

        AccountRepository repository = new AccountRepository();
        repository.store( accounts );

        List<Account> persistedAccounts = repository.findByPendingGeolocationRequest();

        System.assert( persistedAccounts.size() > 0, 'Expected almost one row' );
    }
}