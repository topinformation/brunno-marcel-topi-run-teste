/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GeolocationAddressFutureCalloutTest {

    @isTest
    static void givenAccountsWithAddressWhenSuccessCallFutureCalloutThenUpdatedGeolocationRequestedToTrue() {
        String payload = '{ "message": "sucess" }';
        Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 200 ) );

        Account account = AccountFixtureFactory.newDefaultAccount();
        account.Name = 'Teste Callout Future';
        account.Id = null;
        
        List<Account> accounts = new List<Account>(); 
        accounts.add( account );

        AccountFixtureFactory.create( accounts );

        Test.startTest();
        GeolocationAddressMappingFutureCallout.sendAddressToGeolacationAddressMapping( new AccountFilter().parseToIds( accounts ) );
        Test.stopTest();

        accounts.clear();
        accounts = AccountFixtureFactory.findByName( 'Teste Callout Future' );

        System.assert( accounts[0].GeolocationRequested__c , 'Expected true');
    }

    // @isTest
    // static void givenAccountsWithAddressWhenFailCallFutureCalloutThenUpdatedGeolocationRequestedToFalse() {
    //     String payload = '[{ "errorCode": "APEX_ERROR", "message": "Error Message" }]';
    //     Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 400 ) );

    //     Account account = AccountFixtureFactory.newDefaultAccount();
    //     account.Name = 'Teste Callout Future';
    //     account.Id = null;
        
    //     List<Account> accounts = new List<Account>(); 
    //     accounts.add( account );

    //     AccountFixtureFactory.create( accounts );

    //     Test.startTest();
    //     try {
    //         GeolocationAddressMappingFutureCallout.sendAddressToGeolacationAddressMapping( new AccountFilter().parseToIds( accounts ) );
    //         // System.assert( false, 'CalloutException expected' );
    //     } catch ( CalloutException ex ) {
    //         System.assert( true );
    //     }
        
    //     accounts.clear();
    //     accounts = AccountFixtureFactory.findByName( 'Teste Callout Future' );

    //     System.assert( true );
    //     Test.stopTest();
    // }
}