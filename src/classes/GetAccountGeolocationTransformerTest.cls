/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GetAccountGeolocationTransformerTest {

    @isTest
    static void givenAccountsWhenParseToGeolocationRequestThenCheckIdsValues() {
        List<Account> accounts = new List<Account>();
        accounts.add(AccountFixtureFactory.newDefaultAccount());

        GetAccountGeolocationAddressRequest geolocationRequest = new GetAccountGeolocationAddressTransformer().accountToGeolocationRequest( accounts );

        System.assertEquals( geolocationRequest.externalIds[0], accounts[0].Id, 'Expected equals Ids' );
    }

    @isTest
    static void givenSucessResponseWhenParseToAccounThenCheckValues() {
        List<Account> accounts = new List<Account>();
        accounts.add( AccountFixtureFactory.newDefaultAccount() );

        List<GeolocationAddressMappingSuccessResponse> successResponses = new List<GeolocationAddressMappingSuccessResponse>();
        
        successResponses.add( GeolocationSuccessResponseFixtureFactory.newDefaultGeolocationAddressMappingSuccessResponse() );
        accounts = new GetAccountGeolocationAddressTransformer().responseToAccount( new Map<Id, Account>( accounts ), successResponses );

        System.assertEquals( Decimal.valueOf( successResponses[0].billingAddress.latitude ), accounts[0].BillingLatitude, 'Expeceted equals values' );
        System.assertEquals( Decimal.valueOf( successResponses[0].billingAddress.longitude ), accounts[0].BillingLongitude, 'Expeceted equals values' );
        System.assertEquals( Decimal.valueOf( successResponses[0].shippingAddress.latitude ), accounts[0].ShippingLatitude, 'Expeceted equals values' );
        System.assertEquals( Decimal.valueOf( successResponses[0].shippingAddress.longitude ), accounts[0].ShippingLongitude, 'Expeceted equals values' );
    }

    @isTest
    static void givenWhenThen3() {
        List<Account> accounts = new List<Account>();
        accounts.add( AccountFixtureFactory.newDefaultAccount() );

        List<GeolocationAddressMappingErrorResponse> errorResponses = new List<GeolocationAddressMappingErrorResponse>();

        errorResponses.add( GeolocationErrorResponseFixtureFactory.newDefaultGeolocationAddressMappingErrorResponse() );
        accounts = new GetAccountGeolocationAddressTransformer().responseToAccount( new Map<Id, Account>( accounts ), errorResponses );

        System.assertEquals( 'Error Message', accounts[0].GeolocationErrorMessage__c, 'Expeceted equals erro message' );
        System.assert( accounts[0].GeolocationRequested__c, 'Expected true to request geolocation again' );
    }
}