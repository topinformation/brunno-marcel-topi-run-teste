/** 
 * @author Brunno Marcel - TOPi
 */
public class OauthProtocolRequestBuild {

    private static final String END_POINT = 'https://login.salesforce.com/services/oauth2/token';

    public HttpRequest buildRequest() {
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint( END_POINT );
        request.setMethod( 'POST' );
        request.setHeader ('Content-Type', 'application/x-www-form-urlencoded');
        request.setBody ( buildPostFormParameters() );

        return request;
    }

    private String buildPostFormParameters() {
        Map<String,String> oauthRequest = bodyValues();
        String parameters = '';

        for ( String parameterName : oauthRequest.keySet() ) {
            if ( oauthRequest.get( parameterName ) == null ) {
                continue;
            }
            parameters += parameterName + '=' + EncodingUtil.urlEncode( oauthRequest.get( parameterName ), 'UTF-8') + '&';
        }

        return parameters;
    }

    private Map<String, String> bodyValues() {
        Map<String, String> requestBody = new Map<String, String>();

        requestBody.put( 'client_id', '3MVG9l2zHsylwlpS6h2vTmlmUGQBhdenOwDRCOFn28Edf9ajwCJ3THJs1OvxrZPVNucENmEJb.7paFUCK3Kqr' );
        requestBody.put( 'client_secret', 'BFC7A19775AA64B697A6F712F62E2D0A671321BBA2FAE18973580F4557F5FC68' );
        requestBody.put( 'redirect_uri', 'http://localhost' );
        requestBody.put( 'grant_type', 'password' );
        requestBody.put( 'username', 'integration.user@topirun.com' );
        requestBody.put( 'password', '@TopiRun2021');

        //teste não cobre a linha 25 pq não tem chave com valor nulo
        requestBody.put( 'cobertura100%', null );

        return requestBody;
    }
}