/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountFixtureFactory {

    public static Account newAccount( String name, String cpf ) {
        return new Account( Name = name, Cpf__c = cpf );
    }

    public static Account create( String name, String cpf ) {
        return create( newAccount( name, cpf ) );
    }

    public static Account create( Account account ) {
        insert account;
        return account;
    }

    public static List<Account> create( List<Account> accounts ) {
        insert accounts;
        return accounts;
    }

    public static Account newDefaultAccount() {
        String payload = '{"attributes":{"type":"Account","url":"/services/data/v51.0/sobjects/Account/0015Y00002bBoIvQAK"},"Id":"0015Y00002bBoIvQAK","Name":"Conta Cpf Cnpj","BillingStreet":"Main Street","BillingPostalCode":"84030-030","ShippingStreet":"Main Street","ShippingPostalCode":"84030-030","Cpf__c":"174.973.340-42","Cnpj__c":"42.029.099/0001-02"}';
        return fromJson( payload );
    }

    public static Account newDefaultAccountUnformatted() {
        String payload = '{"attributes":{"type":"Account","url":"/services/data/v51.0/sobjects/Account/0015Y00002bBoIvQAK"},"Id":"0015Y00001bBoIvQAK","Name":"Conta Cpf Cnpj","BillingStreet":"Main Street","BillingPostalCode":"84030030","ShippingStreet":"Main Street","ShippingPostalCode":"84030030","Cpf__c":"17497334042","Cnpj__c":"42029099000102"}';
        return fromJson( payload );
    }
    
    public static Account fromJson( String payload ) {
        return ( Account ) JSON.deserialize( payload, Account.class );
    }

    public static List<Account> findByName( String name ){
        return [ SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                        BillingCountry, BillingAddress, ShippingStreet, ShippingCity, ShippingState,
                        ShippingPostalCode, ShippingCountry, ShippingAddress, ShippingLongitude, ShippingLatitude,
                        BillingLongitude, BillingLatitude, Cnpj__c, Cpf__c, GeolocationRequested__c, GeolocationErrorMessage__c 
                        FROM Account
                        WHERE Name = :name ];
     }
}