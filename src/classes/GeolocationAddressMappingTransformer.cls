/**
 * @author Brunno Marcel -TOPi
 */
public class GeolocationAddressMappingTransformer {

    public GeolocationAddressMappingRequest accountToGeolocationRequest( Account account ) {
        GeolocationAddressMappingRequest request = new GeolocationAddressMappingRequest();

        request.name = account.Name;
        request.externalId = account.Id;
        request.billingAddress.street = account.BillingStreet;
        request.billingAddress.city = account.BillingCity;
        request.billingAddress.zipCode = account.BillingPostalCode;
        request.billingAddress.state = account.BillingState;
        request.billingAddress.country = account.BillingCountry;
        request.shippingAddress.street = account.ShippingStreet;
        request.shippingAddress.city = account.ShippingCity;
        request.shippingAddress.zipCode = account.ShippingPostalCode;
        request.shippingAddress.state = account.ShippingState;
        request.shippingAddress.country = account.ShippingCountry;

        return request;
    }
    
    public Account responseToAccount( Account account, List<GeolocationAddressMappingErrorResponse> errorResponses ) {
        account.GeolocationErrorMessage__c = errorResponses[0].message;

        return account;
    }
}