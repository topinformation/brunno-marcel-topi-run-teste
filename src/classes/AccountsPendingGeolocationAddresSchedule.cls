/** 
 * @author Brunno Marcel - TOPi
 */
public class AccountsPendingGeolocationAddresSchedule implements Schedulable {

    public void execute(SchedulableContext ctx) {
        List<Account> accounts = new AccountRepository().findByPendingGeolocationRequest();

        GetGeolocationAddressFutureCallout.getAccountGeolocationAddress( new AccountFilter().parseToIds( accounts ) );
    }
}