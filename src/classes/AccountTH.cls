/** 
 * @author Brunno Marcel - TOPi
 */
public virtual class AccountTH extends TriggerHandler {

    List<Account> newAccounts;
    Map<Id, Account> oldAccounts;
    AccountFilter filter;
    AccountValidator validator;
    AccountFormatter formatter;

    public AccountTH(){
        this( (List<Account>) Trigger.new, (Map<Id, Account>) Trigger.oldMap );
    }

    public AccountTH( List<Account> newAccounts, Map<Id, Account> oldAccounts ) {
        this.newAccounts = newAccounts;
        this.oldAccounts = oldAccounts;

        this.filter = new AccountFilter();
        this.validator = new AccountValidator();
        this.formatter = new AccountFormatter();
    }

    virtual
    override
    public void beforeInsert() {
        validateCpfCnpj();
        formatData();
    }

    virtual
    override
    public void beforeUpdate() {
        validateCpfCnpj();
        formatData();
    }

    virtual
    override
    public void afterInsert() {
        sendAnddressToGeolocationSystem();
    }
    
    virtual
    override
    public void afterUpdate() {
        sendAnddressToGeolocationSystem();
    }

    virtual
    public void validateCpfCnpj(){
        validator.validateCpfCnpj( filter.filterByNotEmptyCpfOrCnpj( newAccounts ) );
    }

    virtual
    public void formatData(){
        formatter.formatData( newAccounts );
    }

    virtual
    public void sendAnddressToGeolocationSystem(){
        Set<Account> filteredAccounts = new Set<Account>();
        filteredAccounts.addAll( filter.filterByNewBillingAddress( newAccounts, oldAccounts ) );
        filteredAccounts.addAll( filter.filterByNewShippingAddress( newAccounts, oldAccounts ) );

        if ( !filteredAccounts.isEmpty() ){
            GeolocationAddressMappingFutureCallout.sendAddressToGeolacationAddressMapping( filter.parseToIds( filteredAccounts ) );
            scheduleToRequestGeolocationAddressPending();
        }
    }

    virtual 
    public void scheduleToRequestGeolocationAddressPending(){
        List<CronJobDetail> jobs = [ SELECT Id, Name, JobType FROM CronJobDetail WHERE Name LIKE 'GetGeolocation%' ];
        if ( jobs.size() == 0) {
            System.schedule( 'GetGeolocationMin00', '0 00 * * * ?', new AccountsPendingGeolocationAddresSchedule() );
            System.schedule( 'GetGeolocationMin20', '0 20 * * * ?', new AccountsPendingGeolocationAddresSchedule() );
            System.schedule( 'GetGeolocationMin40', '0 40 * * * ?', new AccountsPendingGeolocationAddresSchedule() );
        }
    }
}