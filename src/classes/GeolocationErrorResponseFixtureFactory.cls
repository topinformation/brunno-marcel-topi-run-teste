/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GeolocationErrorResponseFixtureFactory {

    public static GeolocationAddressMappingErrorResponse newDefaultGeolocationAddressMappingErrorResponse() {
        String payload = '{ "errorCode": "APEX_ERROR", "message": "Error Message" }';
        return fromJson( payload );
    }

    public static GeolocationAddressMappingErrorResponse fromJson( String payload ) {
        return ( GeolocationAddressMappingErrorResponse ) JSON.deserialize( payload, GeolocationAddressMappingErrorResponse.class );
    }
}