/**
 * @author Brunno Marcel -TOPi
 */
public class GeolocationAddressMappingErrorResponse {

    public String errorCode;
    public String message;
}