/** 
 * @author Brunno Marcel - TOPi
 */
public class GeolocationAddressMappingFutureCallout {

    @future( callout = true )
    public static void sendAddressToGeolacationAddressMapping( List<String> accountIds ) {

        AccountRepository accountRepository = new AccountRepository();
        List<Account> accounts = accountRepository.findByIds( accountIds );

        for (Account account : accounts) {
            new GeolocationAddressMappingOutboundService().sendToGeolocationAddressMapping( account );
        }

        accountRepository.store( accounts );
    }
}