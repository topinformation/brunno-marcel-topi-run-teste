/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class CepFormatterTest {

    @isTest
    public static void givenCepWhenPutMaskThenReturnCepWithMask(){
        String formatedCep = CepFormatter.withMask( '00000000' );
        System.assertEquals( '00000-000', formatedCep, 'Expeceted formated values' );
    }

    @isTest
    public static void givenInvalidCepWhenPutMaskThenTrowInvalidInputException(){
        try {
            CepFormatter.withMask( '0000000' );
            System.assert( false, 'It should throw exception' );
        } catch (CepFormatter.InvalidInputException ex) {
            System.assert( ex.getMessage() == 'CEP should have length 8 numbers', 'Expeceted equals values' );
        }
    }

    @isTest
    public static void givenCepWhenRemoveMaskThenReturnCepWithoutMask(){
        String formatedCep = CepFormatter.withoutMask( '00000-000' );
        System.assertEquals( '00000000', formatedCep, 'Expeceted unformated values' );
    }

    @isTest
    public static void givenNullCepWhenRemoveMaskThenReturnCepWithoutMask(){
        String formatedCep = CepFormatter.withoutMask( null );
        System.assertEquals( '', formatedCep, 'Expeceted blank values' );
    }
}