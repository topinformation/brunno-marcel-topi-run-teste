/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GeolocationAddressOutboundServiceTest {

    @isTest
    static void givenAccountWithAddressWhenSuccessCallServiceThenReturnAccountWithGeolocationRequestedToTrue() {
        String payload = '{ "message": "sucess" }';
        Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 200 ) );

        Account account = AccountFixtureFactory.newDefaultAccount();

        Test.startTest();
        Account accountSync = new GeolocationAddressMappingOutboundService().sendToGeolocationAddressMapping( account );
        Test.stopTest();

        System.assert( accountSync.GeolocationRequested__c, 'Expected true' );
    }

    // @isTest
    // static void givenAccountWithAddressWhenFailCallServiceThenReturnAccountWithGeolocationRequestedToFalse() {
    //     String payload = '[{ "errorCode": "APEX_ERROR", "message": "Error Message" }]';
    //     Test.setMock( HttpCalloutMock.class, new TopiHttpCalloutMock( payload, 400 ) );

    //     Account account = AccountFixtureFactory.newDefaultAccount();

    //     Test.startTest();
    //     Account accountSync = new GeolocationAddressMappingOutboundService().sendToGeolocationAddressMapping( account );
    //     Test.stopTest();

    //     System.assert( !accountSync.GeolocationRequested__c );
    // }
}