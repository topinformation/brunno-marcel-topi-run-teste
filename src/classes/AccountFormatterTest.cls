/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountFormatterTest {

    @isTest
    public static void givenAccountWithoutValuesToFormatWhenFormatDatasThenReturnNullValues() {
        Account account = AccountFixtureFactory.newAccount( 'Teste 01', '' );

        List<Account> accounts = new List<Account>();
        accounts.add( account );

        List<Account> formatedAccounts = new AccountFormatter().formatData( accounts );

        System.assert( String.isEmpty( formatedAccounts.get(0).Cpf__c ), 'Supposed to be blank' );
        System.assert( String.isEmpty( formatedAccounts.get(0).Cnpj__c ), 'Supposed to be null' );
        System.assert( formatedAccounts.get(0).BillingPostalCode == null, 'Supposed to be null' );
        System.assert( formatedAccounts.get(0).ShippingPostalCode == null, 'Supposed to be null' );
    }

    @isTest
    public static void givenAccountWithValuesToFormatWhenFormatDatasThenReturnValuesFormatted() {
        Account account = AccountFixtureFactory.newDefaultAccountUnformatted();

        List<Account> accounts = new List<Account>();
        accounts.add( account );

        new AccountFormatter().formatData( accounts );

        System.assertEquals( '174.973.340-42', accounts.get(0).Cpf__c, 'Supposed to be 174.973.340-42' );
        System.assertEquals( '42.029.099/0001-02', accounts.get(0).Cnpj__c, 'Supposed to be 42.029.099/0001-02' );
        System.assertEquals( '84030-030', accounts.get(0).BillingPostalCode, 'Supposed to be 84030-030' );
        System.assertEquals( '84030-030', accounts.get(0).ShippingPostalCode, 'Supposed to be 84030-030' );
    }
}