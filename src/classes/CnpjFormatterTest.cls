/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class CnpjFormatterTest {

    @isTest
    public static void givenCnpjWhenPutMaskThenReturnCnpjWithMask(){
        String formatedCnpj = CnpjFormatter.withMask( '00000000000000' );
        System.assertEquals( '00.000.000/0000-00', formatedCnpj, 'Expeceted formatted values' );
    }

    @isTest
    public static void givenInvalidCnpjWhenPutMaskThenTrowInvalidInputException(){
        try {
            CnpjFormatter.withMask( '000000000000' );
            System.assert( false, 'It should throw exception' );
        } catch (CnpjFormatter.InvalidInputException ex) {
            System.assert( ex.getMessage() == 'CNPJ should have length 14 numbers', 'Expeceted equals values' );
        }
    }

    @isTest
    public static void givenCnpjWhenRemoveMaskThenReturnCnpjWithoutMask(){
        String formatedCnpj = CnpjFormatter.withoutMask( '00.000.000/0000-00' );
        System.assertEquals( '00000000000000', formatedCnpj, 'Expeceted unformated values' );
    }

    @isTest
    public static void givenNullCnpjWhenRemoveMaskThenReturnCnpjWithoutMask(){
        String formatedCnpj = CnpjFormatter.withoutMask( null );
        System.assertEquals( '', formatedCnpj, 'Expeceted blank values' );
    }
}