/** 
 * @author Brunno Marcel - TOPi
 */
public class GeolocationAddressMappingOutboundService {
    
    public static final String END_POINT = 'https://bisso-topirun-20-dev-ed.my.salesforce.com/services/apexrest/api/account/geolocation';
    public GeolocationAddressMappingTransformer transformer;
    
    public GeolocationAddressMappingOutboundService() {
        this.transformer = new GeolocationAddressMappingTransformer();
    }

    public Account sendToGeolocationAddressMapping( Account account ) {
        Http http = new Http();
        HttpResponse response = http.send( generateRequest( account ) );
        
        if ( response.getStatusCode() != 200 ) {
            List<GeolocationAddressMappingErrorResponse> errorResponses = ( List<GeolocationAddressMappingErrorResponse> ) JSON.deserialize( response.getBody(), List<GeolocationAddressMappingErrorResponse>.class );
            return transformer.responseToAccount( account, errorResponses );
        }

        account.GeolocationRequested__c = true;
        return account;
    }

    public HttpRequest generateRequest( Account account ) {
        HttpRequest request = new HttpRequest();
        String accessToken  = new OauthProtocolOutboundService().requestToken();

        request.setEndpoint( END_POINT );
        request.setMethod( 'PUT' );
        request.setHeader( 'Authorization', 'Bearer ' + accessToken  );
        request.setHeader( 'Content-Type', 'application/json' );
        request.setBody( JSON.serialize( transformer.accountToGeolocationRequest( account ) ) );

        return request;
    }
}