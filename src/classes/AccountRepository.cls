/**
 * @author Brunno Marcel - TOPi
 */
public class AccountRepository {

    public List<Account> store( List<Account> accounts ){
        update accounts;
        return accounts;
    }

    public List<Account> findByIds( List<String> ids ) {
        return [ SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingAddress, 
                        ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ShippingAddress, 
                        GeolocationRequested__c, GeolocationErrorMessage__c 
                        FROM Account 
                        WHERE Id in :ids ];
    }

    public List<Account> findByPendingGeolocationRequest(){
        return [ SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                        BillingCountry, BillingAddress, ShippingStreet, ShippingCity, ShippingState,
                        ShippingPostalCode, ShippingCountry, ShippingAddress, ShippingLongitude, ShippingLatitude,
                        BillingLongitude, BillingLatitude, Cnpj__c, Cpf__c, GeolocationRequested__c, GeolocationErrorMessage__c 
                        FROM Account
                        WHERE GeolocationRequested__c = true ];
     }
}