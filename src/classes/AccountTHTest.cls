/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountTHTest {

    @isTest
    public static void givenAccountWithCpfWhenBeforeAndAfterInsertThenValidateCpfAndPutMask(){
        List<Account> newAccounts = new List<Account>();
        Map<Id, Account> oldAccounts = new Map<Id, Account>();
        Account account = AccountFixtureFactory.newAccount( 'ABC', '17497334042' );

        newAccounts.add( account );

        AccountTH accountTH = new AccountTH( newAccounts, oldAccounts );

        accountTH.beforeInsert();
        accountTH.afterInsert();

        System.assertEquals( '174.973.340-42', newAccounts[0].Cpf__c, 'Expected equals values' );
    }

    @isTest
    public static void givenAccountWithCpfWhenBeforeAndAfterUpdateThenValidateCpfAndPutMask(){
        List<Account> newAccounts = new List<Account>();
        Map<Id, Account> oldAccounts = new Map<Id, Account>();
        Account account = AccountFixtureFactory.newDefaultAccountUnformatted();

        oldAccounts.put( account.Id, account );
        account.Name = 'Updated Account';
        newAccounts.add( account );

        AccountTH accountTH = new AccountTH( newAccounts, oldAccounts );
        accountTH.beforeUpdate();
        accountTH.afterUpdate();

        System.assertEquals( '174.973.340-42', newAccounts[0].Cpf__c, 'Expected equals values' );
    }

    @isTest
    public static void givenAccountTrriggerHandlerWhenCreateScheduleThenScheduleCreated(){
        AccountTH accountTH = new AccountTH();

        accountTH.scheduleToRequestGeolocationAddressPending();

        System.assert( true, 'Dont know how to test' );
    }
}