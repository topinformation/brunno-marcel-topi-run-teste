/** 
 * @author Brunno Marcel - TOPi
 */
public class TopiHttpCalloutMock implements HttpCalloutMock{

    String payload;
    Integer statusCode;

    public TopiHttpCalloutMock( String payload, Integer statusCode ) {
        this.payload = payload;
        this.statusCode = statusCode;
    }

    public HttpResponse respond( HttpRequest request ) {
        HttpResponse response = new HttpResponse();

        response.setBody( payload );
        response.setStatusCode( statusCode );

        return response;
    }
}