/**
 * @author Brunno Marcel - TOPi
 */
@isTest
public class FilterTest {

    @isTest
    public static void givenAccountsWhenParseToIdsListThenReturnIdsList() {
        List<Account> accounts = new List<Account>();
        accounts.add( AccountFixtureFactory.newDefaultAccount() );

        List<String> accountIds = new AccountFilter().parseToIds( accounts );

        System.assertEquals( accountIds[0], accounts[0].Id, 'Expeceted equals ids' );
        System.assert( accountIds.size() == 1, 'Expeceted one row' );
    }
}