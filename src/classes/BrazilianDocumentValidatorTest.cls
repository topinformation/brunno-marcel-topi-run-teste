/**
 * @author Brunno Marcel - TOPi
 */
@isTest
private class BrazilianDocumentValidatorTest {

    @isTest
    static void givenValidCnpjWhenValidateThenReturnTrue() {
        String cnpj = '09810803000186';

        System.assert( new BrazilianDocumentValidator().isCnpj( cnpj ), 'Expeceted validated cnpj' );
    }

    @isTest
    static void givenInvalidCnpjWhenValidateThenReturnFalse() {
        String cnpj = '00000000000000';

        System.assert( !( new BrazilianDocumentValidator().isCnpj( cnpj ) ), 'Expeceted not validated cnpj' );
    }

    @isTest
    static void givenValidCpfWhenValidateThenReturnTrue() {
        String cpf = '79284783070';

        System.assert( new BrazilianDocumentValidator().isCpf( cpf ), 'Expeceted validated cpf' );
    }

    @isTest
    static void givenInvalidCpfWhenValidateThenReturnFalse() {
        String cpf = '0000000000';

        System.assert( !( new BrazilianDocumentValidator().isCpf( cpf ) ), 'Expeceted not validated cpf' );
    }
}