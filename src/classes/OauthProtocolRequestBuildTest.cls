/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class OauthProtocolRequestBuildTest {

    @isTest
    public static void givenRequestWhenBuildRequestThenCheckEndPointAndMethod() {
        HttpRequest request = new OauthProtocolRequestBuild().buildRequest();

        System.assertEquals( 'https://login.salesforce.com/services/oauth2/token', request.getEndpoint(), 'right endpoint' );
        System.assertEquals( 'POST', request.getMethod(), 'right method');
    }
}