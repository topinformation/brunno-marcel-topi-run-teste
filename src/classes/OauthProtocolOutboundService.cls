/** 
 * @author Brunno Marcel - TOPi
 */
public class OauthProtocolOutboundService {
    OauthProtocolRequestBuild requestBuilder;

    public OauthProtocolOutboundService(){
        requestBuilder = new OauthProtocolRequestBuild();
    }

    public String requestToken() {
        Http http = new Http();
        HttpResponse response = http.send( requestBuilder.buildRequest() );
        
        if( response.getStatusCode() >= 400 ){
            throw new CalloutException('Cannot get Token');
        }
        
        OauthTokenResponse authToken = ( OauthTokenResponse ) JSON.deserialize( response.getBody(), OauthTokenResponse.class );
        return authToken.access_token;
    }
}