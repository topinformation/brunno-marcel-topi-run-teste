/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class GeolocationAddressMappingRequestTest {

    @isTest
    static void givenGeolocationAddressMappingRequestWhenInsertNewZipcodeThenCheckInstanceZipCodeValue() {
        GeolocationAddressMappingRequest geolocationRequest = new GeolocationAddressMappingRequest();

        geolocationRequest.billingAddress.zipCode = '84020-020';

        System.assertEquals( '84020-020', geolocationRequest.billingAddress.zipCode, 'Expeceted equals values' );
    }
}