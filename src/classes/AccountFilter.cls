/**
 * @author Brunno Marcel - TOPi
 */
public class AccountFilter extends Filter {

    public List<Account> filterByNotEmptyCpfOrCnpj( List<Account> newAccounts ) {
        List<Account> filteredAccounts = new List<Account>();
        
        for (Account account : newAccounts) {
            if ( String.isEmpty( account.Cpf__c ) && String.isEmpty( account.Cnpj__c ) ){
                continue;
            }

            filteredAccounts.add( account );
        }

        return filteredAccounts;
    }

    public List<Account> filterByNewBillingAddress( List<Account> newAccounts, Map<Id, Account> oldAccounts ) {
        List<Account> filteredAccounts = new List<Account>();
        
        for (Account account : newAccounts) {
            if( String.isEmpty( account.BillingStreet ) && String.isEmpty( account.BillingPostalCode ) ){
                continue;
            }

            if( oldAccounts == null || oldAccounts.isEmpty() ){
                filteredAccounts.add( account );
                continue;
            }

            Account oldAccount = oldAccounts.get( account.Id );

            if( account.BillingStreet == oldAccount.BillingStreet && 
                account.BillingCity == oldAccount.BillingCity &&
                account.BillingPostalCode == oldAccount.BillingPostalCode &&
                account.BillingState == oldAccount.BillingState &&
                account.BillingCountry == oldAccount.BillingCountry ) {
                continue;
            }

            filteredAccounts.add( account );
        }

        return filteredAccounts;
    }

    public List<Account> filterByNewShippingAddress( List<Account> newAccounts, Map<Id, Account> oldAccounts ) {
        List<Account> filteredAccounts = new List<Account>();
        
        for (Account account : newAccounts) {
            if( String.isEmpty( account.ShippingStreet ) && String.isEmpty( account.ShippingPostalCode ) ) {
                continue;
            }

            if( oldAccounts == null || oldAccounts.isEmpty() ){
                filteredAccounts.add( account );
                continue;
            }

            Account oldAccount = oldAccounts.get( account.Id );

            if( account.ShippingStreet == oldAccount.ShippingStreet && 
                account.ShippingCity == oldAccount.ShippingCity &&
                account.ShippingPostalCode == oldAccount.ShippingPostalCode &&
                account.ShippingState == oldAccount.ShippingState &&
                account.ShippingCountry == oldAccount.ShippingCountry  ) {
                continue;
            }

            filteredAccounts.add( account );
        }

        return filteredAccounts;
    }

    public List<String> parseToIds( Set<Account> params ) {
        List<String> ids = new List<String>();

        for (Account param : params) {
            ids.add( param.Id );
        }

        return ids;
    }
}