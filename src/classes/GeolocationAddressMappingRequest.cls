/**
 * @author Brunno Marcel
 */
public class GeolocationAddressMappingRequest {

    public String name;
    public String externalId;
    public Adress billingAddress;
    public Adress shippingAddress;

    public GeolocationAddressMappingRequest() {
        this.billingAddress = new GeolocationAddressMappingRequest.Adress();
        this.shippingAddress = new GeolocationAddressMappingRequest.Adress();
    }

    public class Adress {
        public String street;
        public String city;
        public String zipCode;
        public String state;
        public String country;
    }
}