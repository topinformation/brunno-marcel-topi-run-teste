/** 
 * @author Brunno Marcel - TOPi
 */
@isTest
public class AccountsPendingGeolocationScheduleTest {

    @isTest
    public static void givenScheduleWhenExecuteJob() {
        Test.StartTest();
        SchedulableContext sc = null;
        AccountsPendingGeolocationAddresSchedule tsc = new AccountsPendingGeolocationAddresSchedule();
        tsc.execute(sc);
        Test.stopTest();

        System.assert( true, 'I dont know how to check' );
    }
    
    @isTest
    public static void givenScheduleWhenInitJob() {
        Test.StartTest();
        System.schedule( 'AccountsPendingGeolocationAddresSchedule', '0 20 * * * ?', new AccountsPendingGeolocationAddresSchedule() );
        Test.stopTest(); 
        
        System.assert( true, 'I dont know how to check' );
    }
}